#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/cdev.h>
#include <linux/device.h>
#include <asm/uaccess.h>
#include <linux/io.h>

MODULE_LICENSE("GPL");

#define NAME "screenBuf"
#define HEADER NAME ": "

#define REG_BASE 0x40000000
#define AXI_REG_SIZE sizeof(u32)
#define REG_R0 (0*AXI_REG_SIZE)
#define REG_R1 (1*AXI_REG_SIZE)
#define REG_R2 (2*AXI_REG_SIZE)
#define REG_R3 (3*AXI_REG_SIZE)

struct state
{
	dev_t dev;
	bool dev_alloc;
	struct class* devClass;
	struct device* devFile;
	struct cdev cdev;
	bool cdev_alloc;
	int* regs;
	bool sysfs;
};

struct state state =
{
	.devClass = NULL,
};

static ssize_t screenBuf_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	char* msg = "hello";

	int cnt = (count < 5) ? count : 5;
	int c = copy_to_user(buf, msg, cnt);
	return cnt - c;
}

static ssize_t screenBuf_write(struct file *filp, const char *buff, size_t len, loff_t *off)
{
	printk (HEADER "Write operation not supported.\n");
	return -EINVAL;
}

static int screenBuf_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int screenBuf_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static struct file_operations screenBuf_fops =
{
	.owner   = THIS_MODULE,
	.read    = screenBuf_read,
	.write   = screenBuf_write,
	.open    = screenBuf_open,
	.release = screenBuf_release
};

static inline int getRegVal(int* base, int off) {return ioread32(base + (off >> 2));}
static inline void setRegVal(int* base, int off, int val) {iowrite32(val, base + (off >> 2)); return;}

static ssize_t r0_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R0);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r0_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(state.regs, REG_R0, tmp);
	return count;
}
static ssize_t r1_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R1);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r1_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(state.regs, REG_R1, tmp);
	return count;
}
static ssize_t r2_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R2);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r3_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R3);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}

static struct kobj_attribute sysfsfiles[] =
{
	__ATTR(r0, 0660, r0_show, r0_store),
	__ATTR(r1, 0660, r1_show, r1_store),
	__ATTR(r2, 0440, r2_show, NULL),
	__ATTR(r3, 0440, r3_show, NULL),
};
static struct attribute* attrs[] =
{
	&sysfsfiles[0].attr,
	&sysfsfiles[1].attr,
	&sysfsfiles[2].attr,
	&sysfsfiles[3].attr,
	NULL,
};
struct attribute_group ag =
{
	.name = "screenBuf",
	.attrs = attrs,
};


void cleanup_screenBuf(void)
{
	if(state.sysfs)
		sysfs_remove_group(&state.devFile->kobj, &ag);
	if(state.regs)
		iounmap(state.regs);
	if(state.cdev_alloc)
		cdev_del(&state.cdev);
	if(!IS_ERR_OR_NULL(state.devFile))
		device_destroy(state.devClass, state.dev);
	if(!IS_ERR_OR_NULL(state.devClass))
		class_destroy(state.devClass);
	if(state.dev_alloc)
		unregister_chrdev_region(state.dev, 1);

	printk("exiting\n");
}

int __init init_screenBuf(void)
{
	int err;

	printk("Loading...\n");

	err = alloc_chrdev_region(&state.dev, 0, 1, NAME);
	if (err < 0)
	{
		printk(KERN_WARNING HEADER "alloc_chrdev_region() failed. (%d)\n", err);
		cleanup_screenBuf();
		return -1;
	}

	state.dev_alloc = true;

	printk(HEADER "Major: %d\n", MAJOR(state.dev));

	state.devClass = class_create(THIS_MODULE, NAME);
	if (IS_ERR_OR_NULL(state.devClass))
	{
		printk(KERN_WARNING HEADER "Error creating device class.\n");
		cleanup_screenBuf();
		return -1;
	}

	state.devFile = device_create(state.devClass, NULL, /* no parent device */
		state.dev, NULL, /* no additional data */
		NAME "%d", MINOR(state.dev));

	if (IS_ERR_OR_NULL(state.devFile))
	{
		err = PTR_ERR(state.devFile);
		printk(KERN_WARNING HEADER "Error %d while trying to create %s%d\n", err, NAME, MINOR(state.dev));
		cleanup_screenBuf();
		return -1;
	}

	cdev_init(&state.cdev, &screenBuf_fops);
	// cdev.owner = THIS_MODULE;
	
	err = cdev_add(&state.cdev, state.dev, 1);
	if (err)
	{
		printk(KERN_WARNING HEADER "Error %d while trying to add %s%d\n", err, NAME, MINOR(state.dev));
		cleanup_screenBuf();
		return -1;
	}

	state.cdev_alloc = true;

	state.regs = ioremap_nocache(REG_BASE, 0xFFFF);
	if(!state.regs)
	{
		printk(HEADER "ioremap failed\n");
		cleanup_screenBuf();
		return -1;
	}

	// Setup sysfs
	if(sysfs_create_group(&state.devFile->kobj, &ag))
	{
		printk(HEADER "sysfs interface failed\n");
		cleanup_screenBuf();
		return -1;
	}
	state.sysfs = 1;

	printk(HEADER "Loaded.\n");

	return 0;
}

void __exit destroy_screenBuf(void)
{
	cleanup_screenBuf();
}

module_init(init_screenBuf);
module_exit(cleanup_screenBuf);
