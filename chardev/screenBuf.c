#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/cdev.h>
#include <linux/device.h>
#include <asm/uaccess.h>

MODULE_LICENSE("GPL");

#define NAME "screenBuf"
#define HEADER NAME ": "

struct state
{
	dev_t dev;
	bool dev_alloc;
	struct class* devClass;
	struct device* devFile;
	struct cdev cdev;
	bool cdev_alloc;
};

struct state state =
{
	.devClass = NULL,
};

static ssize_t screenBuf_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	char* msg = "hello";

	int cnt = (count < 5) ? count : 5;
	int c = copy_to_user(buf, msg, cnt);
	return cnt - c;
}

static ssize_t screenBuf_write(struct file *filp, const char *buff, size_t len, loff_t *off)
{
	printk (HEADER "Write operation not supported.\n");
	return -EINVAL;
}

static int screenBuf_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int screenBuf_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static struct file_operations screenBuf_fops =
{
	.owner   = THIS_MODULE,
	.read    = screenBuf_read,
	.write   = screenBuf_write,
	.open    = screenBuf_open,
	.release = screenBuf_release
};

void cleanup_screenBuf(void)
{
	if(state.cdev_alloc)
		cdev_del(&state.cdev);
	if(!IS_ERR_OR_NULL(state.devFile))
		device_destroy(state.devClass, state.dev);
	if(!IS_ERR_OR_NULL(state.devClass))
		class_destroy(state.devClass);
	if(state.dev_alloc)
		unregister_chrdev_region(state.dev, 1);

	printk("exiting\n");
}

int __init init_screenBuf(void)
{
	int err;

	printk("Loading...\n");

	err = alloc_chrdev_region(&state.dev, 0, 1, NAME);
	if (err < 0)
	{
		printk(KERN_WARNING HEADER "alloc_chrdev_region() failed. (%d)\n", err);
		cleanup_screenBuf();
		return -1;
	}

	state.dev_alloc = true;

	printk(HEADER "Major: %d\n", MAJOR(state.dev));

	state.devClass = class_create(THIS_MODULE, NAME);
	if (IS_ERR_OR_NULL(state.devClass))
	{
		printk(KERN_WARNING HEADER "Error creating device class.\n");
		cleanup_screenBuf();
		return -1;
	}

	state.devFile = device_create(state.devClass, NULL, /* no parent device */
		state.dev, NULL, /* no additional data */
		NAME "%d", MINOR(state.dev));

	if (IS_ERR_OR_NULL(state.devFile))
	{
		err = PTR_ERR(state.devFile);
		printk(KERN_WARNING HEADER "Error %d while trying to create %s%d\n", err, NAME, MINOR(state.dev));
		cleanup_screenBuf();
		return -1;
	}

	cdev_init(&state.cdev, &screenBuf_fops);
	// cdev.owner = THIS_MODULE;
	
	err = cdev_add(&state.cdev, state.dev, 1);
	if (err)
	{
		printk(KERN_WARNING HEADER "Error %d while trying to add %s%d\n", err, NAME, MINOR(state.dev));
		cleanup_screenBuf();
		return -1;
	}

	state.cdev_alloc = true;

	printk(HEADER "Loaded.\n");

	return 0;
}

void __exit destroy_screenBuf(void)
{
	cleanup_screenBuf();
}

module_init(init_screenBuf);
module_exit(cleanup_screenBuf);
