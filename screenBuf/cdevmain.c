#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/dma-mapping.h>
#include <linux/platform_device.h>
#include <asm/uaccess.h>

#include "common.h"
#include "modulemain.h"

static ssize_t screenBuf_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	int bufidx = MINOR(filp->f_inode->i_rdev) - MINOR(state.dev);
	int copied;
	int realCount = min_t(size_t, count, max_t(size_t, 0, sizeof(struct buf) - *f_pos));

	copied = copy_to_user(buf, &state.buf[bufidx].cpuAddr->data[*f_pos], realCount);
	*f_pos += realCount - copied;

	// printk(HEADER "bufidx: %d, realCount: %d, copied: %d, *f_pos: %d\n", bufidx, realCount, copied, (int)*f_pos);

	return realCount - copied;
}

static ssize_t screenBuf_write(struct file *filp, const char *buff, size_t len, loff_t *off)
{
	int bufidx = MINOR(filp->f_inode->i_rdev) - MINOR(state.dev);
	int copied;
	int realCount = min_t(size_t, len, max_t(size_t, 0, sizeof(struct buf) - *off));

	if(!realCount)
		return -ENOSPC;

	copied = copy_from_user(&state.buf[bufidx].cpuAddr->data[*off], buff, realCount);
	*off += realCount - copied;

	// printk(HEADER "bufidx: %d, realCount: %d, copied: %d, *f_pos: %d\n", bufidx, realCount, copied, (int)*f_pos);

	dma_sync_single_for_device(&state.pdev->dev, state.buf[bufidx].dmaAddr, sizeof(struct buf), DMA_TO_DEVICE);

	return realCount - copied;
}

// static loff_t screenBuf_llseek(struct file *filp, loff_t off, int)
// {
// 	return off;
// }

static int screenBuf_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int screenBuf_release(struct inode *inode, struct file *filp)
{
	return 0;
}

struct file_operations screenBuf_fops =
{
	.owner   = THIS_MODULE,
	.read    = screenBuf_read,
	.write   = screenBuf_write,
	// .llseek  = screenBuf_llseek,
	.open    = screenBuf_open,
	.release = screenBuf_release
};

char* screenBuf_devnode(struct device *dev, umode_t *mode)
{
	if(!mode)
		return NULL;

	*mode = 0666;

	return NULL;
}
