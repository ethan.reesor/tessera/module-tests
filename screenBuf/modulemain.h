#include <linux/cdev.h>

#define BUF_SIZE (84*48/8)
#define nBUFs 2

struct buf
{
	char data[BUF_SIZE];
};

struct dmaPoolAddr
{
	struct buf* cpuAddr;
	dma_addr_t dmaAddr;
};

struct state
{
	dev_t dev;
	bool dev_alloc;
	struct class* devClass;
	struct device* devFile[nBUFs];
	struct cdev cdev;
	bool cdev_alloc;
	int* regs;
	bool sysfs;
	struct platform_device* pdev;
	struct dma_pool* bufpool;
	struct dmaPoolAddr buf[nBUFs];
};

extern struct state state;
