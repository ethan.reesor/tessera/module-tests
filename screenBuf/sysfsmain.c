
#include "common.h"
#include "fpga.h"
#include "modulemain.h"

static ssize_t r0_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R0);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r0_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(state.regs, REG_R0, tmp);
	return count;
}
static ssize_t r1_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R1);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r1_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(state.regs, REG_R1, tmp);
	return count;
}
static ssize_t r2_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R2);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r2_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(state.regs, REG_R2, tmp);
	return count;
}
static ssize_t r3_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R3);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r3_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(state.regs, REG_R3, tmp);
	return count;
}
static ssize_t r4_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R4);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r5_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R5);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r6_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R6);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r7_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R7);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r8_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R8);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r9_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R9);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r10_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R10);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r11_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R11);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}


static struct kobj_attribute sysfsfiles[] =
{
	__ATTR(reg_r0, 0660, r0_show, r0_store),
	__ATTR(reg_r1, 0660, r1_show, r1_store),
	__ATTR(reg_r2, 0660, r2_show, r2_store),
	__ATTR(reg_r3, 0660, r3_show, r3_store),
	__ATTR(reg_r4, 0440, r4_show, NULL),
	__ATTR(reg_r5, 0440, r5_show, NULL),
	__ATTR(reg_r6, 0440, r6_show, NULL),
	__ATTR(reg_r7, 0440, r7_show, NULL),
	__ATTR(reg_r8, 0440, r8_show, NULL),
	__ATTR(reg_r9, 0440, r9_show, NULL),
	__ATTR(reg_r10, 0440, r10_show, NULL),
	__ATTR(reg_r11, 0440, r11_show, NULL),
};
static struct attribute* attrs[] =
{
	&sysfsfiles[0].attr,
	&sysfsfiles[1].attr,
	&sysfsfiles[2].attr,
	&sysfsfiles[3].attr,
	&sysfsfiles[4].attr,
	&sysfsfiles[5].attr,
	&sysfsfiles[6].attr,
	&sysfsfiles[7].attr,
	&sysfsfiles[8].attr,
	&sysfsfiles[9].attr,
	&sysfsfiles[10].attr,
	&sysfsfiles[11].attr,
	NULL,
};
struct attribute_group ag =
{
	.name = "screenBuf",
	.attrs = attrs,
};
