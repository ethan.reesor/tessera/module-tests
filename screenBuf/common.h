#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */

MODULE_LICENSE("GPL");

#define NAME "screenBuf"
#define HEADER NAME ": "

#define _xQUOTE(x) #x
#define QUOTE(x) _xQUOTE(x)
