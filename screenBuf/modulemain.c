#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/dmapool.h>

#include "common.h"
#include "modulemain.h"
#include "sysfsmain.h"
#include "fpga.h"
#include "cdevmain.h"

struct state state =
{
	.devClass = NULL,
};

void cleanup_screenBuf(void)
{
	int i;
	if(state.sysfs)
		sysfs_remove_group(&state.devFile[0]->kobj, &ag);
	if(state.regs)
		iounmap(state.regs);
	if(state.cdev_alloc)
		cdev_del(&state.cdev);
	for(i = 0; i < nBUFs; i++)
		if(!IS_ERR_OR_NULL(state.devFile[i]))
			device_destroy(state.devClass, MKDEV(MAJOR(state.dev), MINOR(state.dev) + i));
	if(!IS_ERR_OR_NULL(state.devClass))
		class_destroy(state.devClass);
	if(state.dev_alloc)
		unregister_chrdev_region(state.dev, nBUFs);
	if(state.bufpool)
	{
		for(i = 0; i < nBUFs; i++)
		{
			if(state.buf[i].cpuAddr)
				dma_pool_free(state.bufpool, state.buf[i].cpuAddr, state.buf[i].dmaAddr);
		}
		dma_pool_destroy(state.bufpool);
	}
	if(state.pdev)
		platform_device_unregister(state.pdev);

	printk(HEADER "exiting\n");
}

int __init init_screenBuf(void)
{
	int err, i, k, ret, blar=0xa5;

	printk(HEADER "Loading...\n");
	printk(HEADER "Sanity: %s\n", QUOTE(SANITY));

	state.pdev = platform_device_alloc(NAME "_dev", PLATFORM_DEVID_NONE);
	if(!state.pdev)
	{
		printk(HEADER "Failed to alloc platform device.\n");
		cleanup_screenBuf();
		return -1;
	}

	if(platform_device_add(state.pdev))
	{
		printk(HEADER "Failed to register platform device.\n");
		cleanup_screenBuf();
		return -1;
	}

	// if(dma_declare_coherent_memory(&state.pdev->dev, 0, 0, PAGE_SIZE*10, DMA_MEMORY_MAP) != DMA_MEMORY_MAP)
	// {
	// 	printk(HEADER "Failed to declare coherent memory.\n");
	// 	cleanup_screenBuf();
	// 	return -1;
	// }

	if((ret = dma_set_mask_and_coherent(&state.pdev->dev, DMA_BIT_MASK(28))))
	{
		printk(HEADER "This platform does not support DMA where needed. (error: %d) (has: %016llx) ... ignored\n", ret, dma_get_mask(&state.pdev->dev));
		// cleanup_screenBuf();
		// return -25;

		if((ret = dma_set_coherent_mask(&state.pdev->dev, DMA_BIT_MASK(28))))
		{
			printk(HEADER "This platform does not support coherent DMA where needed. (error: %d) (has: %016llx) ... ignored\n", ret, dma_get_mask(&state.pdev->dev));
			dma_set_coherent_mask(&state.pdev->dev, DMA_BIT_MASK(32));
		}

	}

	printk(HEADER "Setting up dma pool\n");
	state.bufpool = dma_pool_create(NAME "buf", &state.pdev->dev, sizeof(struct buf), AXI_ALIGN, 0);
	if(!state.bufpool)
	{
		printk(HEADER "Failed to create dma pool.\n");
		cleanup_screenBuf();
		return -1;
	}

	printk(HEADER "Allocating from pool\n");
	for(i = 0; i < nBUFs; i++)
	{
		// state.buf[i].cpuAddr = dma_alloc_coherent(&state.pdev->dev, BUF_SIZE, &state.buf[i].dmaAddr, GFP_KERNEL);
		state.buf[i].cpuAddr = dma_pool_alloc(state.bufpool, GFP_KERNEL, &state.buf[i].dmaAddr);
		if(!state.buf[i].cpuAddr)
		{
			printk(HEADER "alloc: %p, dma_mem: %p\n", state.pdev->dev.archdata.dma_ops, state.pdev->dev.dma_mem);
			printk(HEADER "dma_alloc failed\n");
			cleanup_screenBuf();
			return -1;
		}
		printk(HEADER "allocated buf[%d].cpuAddr = %p\n", i, state.buf[i].cpuAddr);
		printk(HEADER "allocated buf[%d].dmaAddr = %08x\n", i, state.buf[i].dmaAddr);
		printk(HEADER "allocated buf[%d] physaddr= %08x\n", i, dma_to_phys(&state.pdev->dev, state.buf[i].dmaAddr));
		for(k = 0; k < 8; k++)
		{
			state.buf[i].cpuAddr->data[k] = 254-k;
		}
		dma_sync_single_for_device(&state.pdev->dev, state.buf[i].dmaAddr, sizeof(struct buf), DMA_TO_DEVICE);
	}

	err = alloc_chrdev_region(&state.dev, 0, nBUFs, NAME);
	if (err < 0)
	{
		printk(KERN_WARNING HEADER "alloc_chrdev_region() failed. (%d)\n", err);
		cleanup_screenBuf();
		return -1;
	}

	state.dev_alloc = true;

	printk(HEADER "Major: %d\n", MAJOR(state.dev));

	state.devClass = class_create(THIS_MODULE, NAME);
	if (IS_ERR_OR_NULL(state.devClass))
	{
		printk(KERN_WARNING HEADER "Error creating device class.\n");
		cleanup_screenBuf();
		return -1;
	}

	state.devClass->devnode = screenBuf_devnode;

	for(i = 0; i < nBUFs; i++)
	{
		dev_t tmp = MKDEV(MAJOR(state.dev), MINOR(state.dev) + i);
		state.devFile[i] = device_create(state.devClass, NULL, /* no parent device */
			tmp, NULL, /* no additional data */
			NAME "%d", MINOR(tmp));
		
		if (IS_ERR_OR_NULL(state.devFile[i]))
		{
			err = PTR_ERR(state.devFile[i]);
			printk(KERN_WARNING HEADER "Error %d while trying to create %s%d\n", err, NAME, MINOR(tmp));
			cleanup_screenBuf();
			return -1;
		}
	}

	cdev_init(&state.cdev, &screenBuf_fops);
	// cdev.owner = THIS_MODULE;
	
	err = cdev_add(&state.cdev, state.dev, nBUFs);
	if (err)
	{
		printk(KERN_WARNING HEADER "Error %d while trying to add %s%d\n", err, NAME, MINOR(state.dev));
		cleanup_screenBuf();
		return -1;
	}

	state.cdev_alloc = true;

	state.regs = ioremap_nocache(REG_BASE, 0x1000);
	if(!state.regs)
	{
		printk(HEADER "ioremap failed\n");
		cleanup_screenBuf();
		return -1;
	}

	// Setup sysfs
	if(sysfs_create_group(&state.pdev->dev.kobj, &ag))
	{
		printk(HEADER "sysfs interface failed\n");
		cleanup_screenBuf();
		return -1;
	}
	state.sysfs = 1;

	printk(HEADER "blar address: %08x\n", virt_to_dma(&state.pdev->dev, &blar));

	printk(HEADER "Loaded.\n");

	return 0;
}

void __exit destroy_screenBuf(void)
{
	cleanup_screenBuf();
}

module_init(init_screenBuf);
module_exit(cleanup_screenBuf);
