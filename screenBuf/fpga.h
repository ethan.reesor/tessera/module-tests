#include <linux/io.h>

#define AXI_ALIGN 64

#define REG_BASE 0x40000000

#define AXI_REG_SIZE sizeof(u32)
#define REG_R0 (0*AXI_REG_SIZE)
#define REG_R1 (1*AXI_REG_SIZE)
#define REG_R2 (2*AXI_REG_SIZE)
#define REG_R3 (3*AXI_REG_SIZE)
#define REG_R4 (4*AXI_REG_SIZE)
#define REG_R5 (5*AXI_REG_SIZE)
#define REG_R6 (6*AXI_REG_SIZE)
#define REG_R7 (7*AXI_REG_SIZE)
#define REG_R8 (8*AXI_REG_SIZE)
#define REG_R9 (9*AXI_REG_SIZE)
#define REG_R10 (10*AXI_REG_SIZE)
#define REG_R11 (11*AXI_REG_SIZE)

static inline int getRegVal(int* base, int off) {return ioread32(base + (off >> 2));}
static inline void setRegVal(int* base, int off, int val) {iowrite32(val, base + (off >> 2)); return;}
