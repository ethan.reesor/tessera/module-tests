#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/cdev.h>
#include <linux/device.h>
#include <asm/uaccess.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/dmapool.h>

MODULE_LICENSE("GPL");

#define NAME "screenBuf"
#define HEADER NAME ": "

#define REG_BASE 0x40000000
#define AXI_REG_SIZE sizeof(u32)
#define REG_R0 (0*AXI_REG_SIZE)
#define REG_R1 (1*AXI_REG_SIZE)
#define REG_R2 (2*AXI_REG_SIZE)
#define REG_R3 (3*AXI_REG_SIZE)
#define REG_R4 (4*AXI_REG_SIZE)
#define REG_R5 (5*AXI_REG_SIZE)
#define REG_R6 (6*AXI_REG_SIZE)
#define REG_R7 (7*AXI_REG_SIZE)
#define REG_R8 (8*AXI_REG_SIZE)
#define REG_R9 (9*AXI_REG_SIZE)
#define REG_R10 (10*AXI_REG_SIZE)
#define REG_R11 (11*AXI_REG_SIZE)

#define AXI_ALIGN 64
#define BUF_SIZE (84*48/8)
#define nBUFs 2

struct buf
{
	char data[BUF_SIZE];
};

struct dmaPoolAddr
{
	struct buf* cpuAddr;
	dma_addr_t dmaAddr;
};

struct state
{
	dev_t dev;
	bool dev_alloc;
	struct class* devClass;
	struct device* devFile[nBUFs];
	struct cdev cdev;
	bool cdev_alloc;
	int* regs;
	bool sysfs;
	struct platform_device* pdev;
	struct dma_pool* bufpool;
	struct dmaPoolAddr buf[nBUFs];
};

struct state state =
{
	.devClass = NULL,
};

static ssize_t screenBuf_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	int bufidx = MINOR(filp->f_inode->i_rdev) - MINOR(state.dev);
	int copied;
	int realCount = min_t(size_t, count, max_t(size_t, 0, sizeof(struct buf) - *f_pos));

	copied = copy_to_user(buf, &state.buf[bufidx].cpuAddr->data[*f_pos], realCount);
	*f_pos += realCount - copied;

	// printk(HEADER "bufidx: %d, realCount: %d, copied: %d, *f_pos: %d\n", bufidx, realCount, copied, (int)*f_pos);

	return realCount - copied;
}

static ssize_t screenBuf_write(struct file *filp, const char *buff, size_t len, loff_t *off)
{
	int bufidx = MINOR(filp->f_inode->i_rdev) - MINOR(state.dev);
	int copied;
	int realCount = min_t(size_t, len, max_t(size_t, 0, sizeof(struct buf) - *off));

	copied = copy_from_user(&state.buf[bufidx].cpuAddr->data[*off], buff, realCount);
	*off += realCount - copied;

	// printk(HEADER "bufidx: %d, realCount: %d, copied: %d, *f_pos: %d\n", bufidx, realCount, copied, (int)*f_pos);

	return realCount - copied;
}

// static loff_t screenBuf_llseek(struct file *filp, loff_t off, int)
// {
// 	return off;
// }

static int screenBuf_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int screenBuf_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static struct file_operations screenBuf_fops =
{
	.owner   = THIS_MODULE,
	.read    = screenBuf_read,
	.write   = screenBuf_write,
	// .llseek  = screenBuf_llseek,
	.open    = screenBuf_open,
	.release = screenBuf_release
};

static inline int getRegVal(int* base, int off) {return ioread32(base + (off >> 2));}
static inline void setRegVal(int* base, int off, int val) {iowrite32(val, base + (off >> 2)); return;}

static ssize_t r0_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R0);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r0_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(state.regs, REG_R0, tmp);
	return count;
}
static ssize_t r1_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R1);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r1_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(state.regs, REG_R1, tmp);
	return count;
}
static ssize_t r2_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R2);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r2_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(state.regs, REG_R2, tmp);
	return count;
}
static ssize_t r3_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R3);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r3_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(state.regs, REG_R3, tmp);
	return count;
}
static ssize_t r4_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R4);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r5_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R5);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r6_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R6);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r7_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R7);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r8_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R8);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r9_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R9);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r10_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R10);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}
static ssize_t r11_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	u32 stat = getRegVal(state.regs, REG_R11);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}


static struct kobj_attribute sysfsfiles[] =
{
	__ATTR(reg_r0, 0660, r0_show, r0_store),
	__ATTR(reg_r1, 0660, r1_show, r1_store),
	__ATTR(reg_r2, 0660, r2_show, r2_store),
	__ATTR(reg_r3, 0660, r3_show, r3_store),
	__ATTR(reg_r4, 0440, r4_show, NULL),
	__ATTR(reg_r5, 0440, r5_show, NULL),
	__ATTR(reg_r6, 0440, r6_show, NULL),
	__ATTR(reg_r7, 0440, r7_show, NULL),
	__ATTR(reg_r8, 0440, r8_show, NULL),
	__ATTR(reg_r9, 0440, r9_show, NULL),
	__ATTR(reg_r10, 0440, r10_show, NULL),
	__ATTR(reg_r11, 0440, r11_show, NULL),
};
static struct attribute* attrs[] =
{
	&sysfsfiles[0].attr,
	&sysfsfiles[1].attr,
	&sysfsfiles[2].attr,
	&sysfsfiles[3].attr,
	&sysfsfiles[4].attr,
	&sysfsfiles[5].attr,
	&sysfsfiles[6].attr,
	&sysfsfiles[7].attr,
	&sysfsfiles[8].attr,
	&sysfsfiles[9].attr,
	&sysfsfiles[10].attr,
	&sysfsfiles[11].attr,
	NULL,
};
struct attribute_group ag =
{
	.name = "screenBuf",
	.attrs = attrs,
};

char* screenBuf_devnode(struct device *dev, umode_t *mode)
{
	if(!mode)
		return NULL;

	*mode = 0666;

	return NULL;
}

void cleanup_screenBuf(void)
{
	int i;
	if(state.sysfs)
		sysfs_remove_group(&state.devFile[0]->kobj, &ag);
	if(state.regs)
		iounmap(state.regs);
	if(state.cdev_alloc)
		cdev_del(&state.cdev);
	for(i = 0; i < nBUFs; i++)
		if(!IS_ERR_OR_NULL(state.devFile[i]))
			device_destroy(state.devClass, MKDEV(MAJOR(state.dev), MINOR(state.dev) + i));
	if(!IS_ERR_OR_NULL(state.devClass))
		class_destroy(state.devClass);
	if(state.dev_alloc)
		unregister_chrdev_region(state.dev, nBUFs);
	if(state.bufpool)
	{
		for(i = 0; i < nBUFs; i++)
		{
			if(state.buf[i].cpuAddr)
				dma_pool_free(state.bufpool, state.buf[i].cpuAddr, state.buf[i].dmaAddr);
		}
		dma_pool_destroy(state.bufpool);
	}
	if(state.pdev)
		platform_device_unregister(state.pdev);

	printk("exiting\n");
}

int __init init_screenBuf(void)
{
	int err, i, k, ret, blar=0xa5;

	printk("Loading...\n");

	state.pdev = platform_device_alloc(NAME "_dev", PLATFORM_DEVID_NONE);
	if(!state.pdev)
	{
		printk(HEADER "Failed to alloc platform device.\n");
		cleanup_screenBuf();
		return -1;
	}

	if(platform_device_add(state.pdev))
	{
		printk(HEADER "Failed to register platform device.\n");
		cleanup_screenBuf();
		return -1;
	}

	// if(dma_declare_coherent_memory(&state.pdev->dev, 0, 0, PAGE_SIZE*10, DMA_MEMORY_MAP) != DMA_MEMORY_MAP)
	// {
	// 	printk(HEADER "Failed to declare coherent memory.\n");
	// 	cleanup_screenBuf();
	// 	return -1;
	// }

	if((ret = dma_set_mask_and_coherent(&state.pdev->dev, DMA_BIT_MASK(28))))
	{
		printk(HEADER "This platform does not support DMA where needed. (error: %d) (has: %016llx) ... ignored\n", ret, dma_get_mask(&state.pdev->dev));
		// cleanup_screenBuf();
		// return -25;

		if((ret = dma_set_coherent_mask(&state.pdev->dev, DMA_BIT_MASK(28))))
		{
			printk(HEADER "This platform does not support coherent DMA where needed. (error: %d) (has: %016llx) ... ignored\n", ret, dma_get_mask(&state.pdev->dev));
			dma_set_coherent_mask(&state.pdev->dev, DMA_BIT_MASK(32));
		}

	}

	printk(HEADER "Setting up dma pool\n");
	state.bufpool = dma_pool_create(NAME "buf", &state.pdev->dev, sizeof(struct buf), AXI_ALIGN, 0);
	if(!state.bufpool)
	{
		printk(HEADER "Failed to create dma pool.\n");
		cleanup_screenBuf();
		return -1;
	}

	printk(HEADER "Allocating from pool\n");
	for(i = 0; i < nBUFs; i++)
	{
		// state.buf[i].cpuAddr = dma_alloc_coherent(&state.pdev->dev, BUF_SIZE, &state.buf[i].dmaAddr, GFP_KERNEL);
		state.buf[i].cpuAddr = dma_pool_alloc(state.bufpool, GFP_KERNEL, &state.buf[i].dmaAddr);
		if(!state.buf[i].cpuAddr)
		{
			printk(HEADER "alloc: %p, dma_mem: %p\n", state.pdev->dev.archdata.dma_ops, state.pdev->dev.dma_mem);
			printk(HEADER "dma_alloc failed\n");
			cleanup_screenBuf();
			return -1;
		}
		printk(HEADER "allocated buf[%d].cpuAddr = %p\n", i, state.buf[i].cpuAddr);
		printk(HEADER "allocated buf[%d].dmaAddr = %08x\n", i, state.buf[i].dmaAddr);
		printk(HEADER "allocated buf[%d] physaddr= %08x\n", i, dma_to_phys(&state.pdev->dev, state.buf[i].dmaAddr));
		for(k = 0; k < 8; k++)
		{
			state.buf[i].cpuAddr->data[k] = 254-k;
		}
	}

	err = alloc_chrdev_region(&state.dev, 0, nBUFs, NAME);
	if (err < 0)
	{
		printk(KERN_WARNING HEADER "alloc_chrdev_region() failed. (%d)\n", err);
		cleanup_screenBuf();
		return -1;
	}

	state.dev_alloc = true;

	printk(HEADER "Major: %d\n", MAJOR(state.dev));

	state.devClass = class_create(THIS_MODULE, NAME);
	if (IS_ERR_OR_NULL(state.devClass))
	{
		printk(KERN_WARNING HEADER "Error creating device class.\n");
		cleanup_screenBuf();
		return -1;
	}

	state.devClass->devnode = screenBuf_devnode;

	for(i = 0; i < nBUFs; i++)
	{
		dev_t tmp = MKDEV(MAJOR(state.dev), MINOR(state.dev) + i);
		state.devFile[i] = device_create(state.devClass, NULL, /* no parent device */
			tmp, NULL, /* no additional data */
			NAME "%d", MINOR(tmp));
		
		if (IS_ERR_OR_NULL(state.devFile[i]))
		{
			err = PTR_ERR(state.devFile[i]);
			printk(KERN_WARNING HEADER "Error %d while trying to create %s%d\n", err, NAME, MINOR(tmp));
			cleanup_screenBuf();
			return -1;
		}
	}

	cdev_init(&state.cdev, &screenBuf_fops);
	// cdev.owner = THIS_MODULE;
	
	err = cdev_add(&state.cdev, state.dev, nBUFs);
	if (err)
	{
		printk(KERN_WARNING HEADER "Error %d while trying to add %s%d\n", err, NAME, MINOR(state.dev));
		cleanup_screenBuf();
		return -1;
	}

	state.cdev_alloc = true;

	state.regs = ioremap_nocache(REG_BASE, 0x1000);
	if(!state.regs)
	{
		printk(HEADER "ioremap failed\n");
		cleanup_screenBuf();
		return -1;
	}

	// Setup sysfs
	if(sysfs_create_group(&state.pdev->dev.kobj, &ag))
	{
		printk(HEADER "sysfs interface failed\n");
		cleanup_screenBuf();
		return -1;
	}
	state.sysfs = 1;

	printk(HEADER "%08x\n", virt_to_dma(&state.pdev->dev, &blar));

	printk(HEADER "Loaded.\n");

	return 0;
}

void __exit destroy_screenBuf(void)
{
	cleanup_screenBuf();
}

module_init(init_screenBuf);
module_exit(cleanup_screenBuf);
