#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */

MODULE_LICENSE("GPL");

int __init init_empty(void)
{
	printk("Hello world 1.\n");

	return 0;
}

void __exit destroy_empty(void)
{
	printk("Goodbye world 1.\n");
}

module_init(init_empty);
module_exit(destroy_empty);
