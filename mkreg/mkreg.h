#define __rw_mode 0660
#define __ro_mode 0440
#define __wo_mode 0220

#define __base(core) core##_BASE
#define __show(name) name##_show
#define __store(name) name##_store
#define __show_impl(core, name, addr) static ssize_t name##_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) { return showReg(__core(base), addr, buf); };
#define __store_impl(core, name, addr) static ssize_t name##_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count) { return storeReg(__core(base), addr, buf, count); };

#define __rw_show(name) __show(name)
#define __ro_show(name) __show(name)
#define __wo_show(name) NULL
#define __rw_store(name) __store(name)
#define __ro_store(name) NULL
#define __wo_store(name) __store(name)

#define __rw_show_impl(core, name, addr) __show_impl(core, name, addr)
#define __ro_show_impl(core, name, addr) __show_impl(core, name, addr)
#define __wo_show_impl(core, name, addr)
#define __rw_store_impl(core, name, addr) __store_impl(core, name, addr)
#define __ro_store_impl(core, name, addr)
#define __wo_store_impl(core, name, addr) __store_impl(core, name, addr)

enum __reg_num {
#define X(core, name, addr, mode) __##name##_num,
REGISTERS
#undef X
};

#define X(core, name, addr, mode) __##mode##_show_impl(core, name, addr)
REGISTERS
#undef X

#define X(core, name, addr, mode) __##mode##_store_impl(core, name, addr)
REGISTERS
#undef X

static struct kobj_attribute sysfsfiles[] =
{
#define X(core, name, addr, mode) __ATTR(name, __##mode##_mode, __##mode##_show(name), __##mode##_store(name)),
REGISTERS
#undef X
};

static struct attribute* attrs[] =
{
#define X(core, name, addr, mode) &sysfsfiles[__##name##_num].attr,
REGISTERS
#undef X
    NULL,
};