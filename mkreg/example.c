#define REGISTERS \
    X(REG, reg_r0, (0*AXI_REG_SIZE), wo) \
    X(REG, reg_r1, (1*AXI_REG_SIZE), ro) \
    X(REG, reg_r2, (2*AXI_REG_SIZE), rw)

static inline int getRegVal(int* base, int off) {return ioread32(base + (off >> 2));}
static inline void setRegVal(int* base, int off, int val) {iowrite32(val, base + (off >> 2)); return;}

static inline ssize_t showReg(int* base, int off, char *buf)
{
	u32 stat = getRegVal(base, off);
	memcpy(buf, &stat, sizeof(u32));
	return sizeof(u32);
}

static inline ssize_t storeReg(int* base, int off, const char *buf, size_t count)
{
	u32 tmp;
	memcpy(&tmp, buf, sizeof(u32));
	setRegVal(base, off, tmp);
	return count;
};

#import "mkreg.h"